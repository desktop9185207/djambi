# Djambi
![Game](./asset/Board.png)

Le jeu en .exe est dans le dossier zip Djambi.zip il faut le lancer a l'intérieur du dossier Djambi Game pour qu'il fonctionne correctement avec les images.

Dans la branche Reporter, il y a une version du jeu avec le Reporter qui est fonctionnel. Il est possible de jouer avec le Reporter en changeant la branche du projet.

## Table des matières

- [Prérequis](#prérequis)
- [Installation](#installation)
- [Compilation](#compilation)
- [Exécution](#exécution)
- [Matériel](#matériel)
- [But du jeu](#but-du-jeu)
- [Les pions](#les-pions)
    - [Le Chef](#le-chef)
    - [Le Militant](#le-militant)
    - [L'Assassin](#lassassin)
    - [Le Reporter](#le-reporter)
    - [Le Diplomate](#le-diplomate)
    - [La Nécromobile](#la-nécromobile)

## Prérequis

Assurez-vous d'avoir installé les éléments suivants :

- Java Development Kit (JDK) version 21 ou supérieure

## Installation

```bash
git clone
cd Djambi
```

# Compilation

Naviguez jusqu'au répertoire racine du projet où se trouvent les fichiers sources Java. Exécutez la commande suivante pour compiler les fichiers sources Java :

```bash
javac -d /bin  src/*.java
```

Si cela ne fonctionne pas, vous pouvez passer par le run&debug de votre IDE préféré.

# Exécution

Pour exécuter le programme, exécutez la commande suivante :

```bash
java -cp bin Main
```

## Matériel

Le jeu se joue sur un plateau de 9×9 cases.
La case centrale est nommée Labyrinthe. Aucune pièce, à part le Chef ne peut s’y arrêter.


## But du jeu
Un peu comme aux échecs, on joue chacun son tour, l'objectif étant d'éliminer tous les rivaux. Deux façons possibles :
- En tuant les Chefs ennemis.
    - Un joueur qui tue un chef s’approprie ses pièces.
- En encerclant les Chefs ennemis.
    - Les pièces d’un Chef encerclé sont récupérées par le 1er joueur à occuper le Labyrinthe. Vous pourrez encercler le chef ennemi par des cadavres. Celà sera expliqué plus tard.

## Les pions

Il en existe deux types :

- Les Tueurs : Chef, Militant, Assassin et Reporter.
- Les Déplaceurs : Diplomate et Nécromobile.

La particularité de ce jeu réside grandement dans le mécanisme suivant. Lorsqu'un pion A tue un pion B d'une couleur adverse, le pion B est retourné face mort et placé sur le plateau, sur un emplacement libre, selon les désirs du joueur détenteur du pion A. 

Les pions se déplacent en diagonales, en lignes ou en colonnes. n'importe quelle pièce vivante ou morte constitue un obstacle infranchissable.

### Le Chef 
![](./asset/chef.png)
- **Type :** Tueur
- **Déplacement :** Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il veut
- **Élimination :** Peut tuer des pièces en occupant leur cases. La pièce tuée est retournée et placée sur n’importe quelle case libre du plateau.
- **Pouvoir :** Peut occuper le Labyrinthe (les autres pièces ne font que le traverser).
Lorsqu’il occupe le labyrinthe, le Chef dispose d’un tour de jeu supplémentaire après chaque joueur.

### Le Militant
![](./asset/militant.png)
- **Type :** Tueur
- **Déplacement :** Contrairement aux autres pièces qui se déplacent d’autant de cases qu’elles veulent, le Militant ne parcourt que 2 cases maximum, dans toutes les directions.
- **Élimination :** Peut tuer des pièces en occupant leur case. La pièce tuée est retournée et placée sur n’importe quelle case libre du plateau.
Le militant est le seul (parmi les tueurs : Chef, Assassin et Reporter) à ne pas pouvoir tuer un Chef.

### L'Assassin
![](./asset/assassin.png)
- **Type :** Tueur
- **Déplacement :** Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il veut
- **Élimination :** Peut tuer des pièces en occupant leur cases. La pièce tuée est retournée et placée sur la case qu’occupait l’Assassin avant son méfait.
- **Pouvoir :** Peut occuper le labyrinthe pour tuer un Chef. Il dispose alors d’un tour supplémentaire pour fuir le labyrinthe (aucune pièce à part le Chef ne peut rester dans le labyrinthe).

### Le Reporter
![](./asset/reporter.png)
- **Type :** Tueur
- **Déplacement :** Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il faut
- **Élimination :** Tue après son déplacement la pièce adjacente à l’un de ses côtés (au choix). Les pièces tuées ne sont pas déplacées. Il n'est pas obligé de tuer. Ne peut tuer s’il est utilisé par un diplomate ennemi.

### Le Diplomate
![](./asset/diplomate.png)
- **Type :** Deplaceur
- **Déplacement :** Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il faut
- **Élimination :** N’élimine pas !
- **Pouvoir :** Lorsque le diplomate atterrit sur une pièce ennemie, cette dernière est alors replacée par le diplomate sur la case libre de son choix. Attention il ne peut agir qu'avec des pièces d'une autre couleur que la sienne.

### La Nécromobile
![](./asset/necromobile.png)
- **Type :** Deplaceur
- **Déplacement :** Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il faut. C'est le seul pion pouvant s'arrêter sur un mort.
- **Élimination :** N’élimine pas !
- **Pouvoir :** Lorsque le diplomate atterrit sur une pièce contenant un mort, cette dernière est alors replacée par le diplomate sur la case libre de son choix. La nécromobile peut venir "nettoyer" le labyrinthe, il dispose alors d’un tour supplémentaire pour fuir le labyrinthe (aucune pièce à part le Chef ne peut rester dans le labyrinthe).
