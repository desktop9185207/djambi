package model;


import javax.swing.ImageIcon;
import java.awt.Color;

public class Assassin extends Pion {

  public Assassin(int posX, int posY, Color team) {
    super(posX, posY, team);
    this.imgPath = "asset/assassin.png";
  }

  @Override
  public void kill(Case oldCase, Case newCase, Pion pion, Board board){
    ImageIcon pionIcon = new ImageIcon(this.getPath());
    pion.setIsAlive();
    this.setPosition(newCase.getPosX(), newCase.getPosY());
    this.setPossibleMoves(null);
    newCase.setBackground(this.getTeam());
    newCase.setIcon(pionIcon);
    newCase.setPion(this);
    oldCase.setIcon(null);
    oldCase.setPion(pion);
    oldCase.setBackground(pion.getTeam());
    board.setSelectedPion(pion);
    
  }

  public void ability() {
  }



}
