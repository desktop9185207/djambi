package model;

import java.awt.Color;
import java.util.ArrayList;

public class Player {

    private String name;
    private ArrayList<Color> colors;


    public Player(String name, ArrayList<Color> colors) {
        this.name = name;
        this.colors = new ArrayList<Color>(colors);
    }

    public String getName() { 
        return this.name;
    }

    public ArrayList<Color> getColors() { 
        return this.colors;
    }

    public void addColor(Color couleur) { 
        colors.add(couleur);
    }
    
    
}
