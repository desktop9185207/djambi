package model;

import java.util.HashMap;

import javax.swing.JButton;

public class Case extends JButton {

    protected Pion pion;
    protected HashMap<String, Integer> position;
    protected int posX;
    protected int posY;



    public Case(int posX, int posY) {
        this.position = new HashMap<String,Integer>();
        this.position.put("X",posX);
        this.position.put("Y",posY);
        this.pion = null;
    }

    public Case(int posX, int posY, Pion pion){
        this.position = new HashMap<String,Integer>();
        this.position.put("X",posX);
        this.position.put("Y",posY);
        this.pion = pion;
    }

    public int getPosX(){
        return this.position.get("X");
    }
    public int getPosY(){
        return this.position.get("Y");
    }

    public HashMap<String, Integer> getPosistion(){
        return this.position;
    }

    public Pion getPion(){
        if(this.pion != null){
            return this.pion;
        }else{
            return null;
        }
        
    }

    public void setPion(Pion pion){
        this.pion = pion;
    }

}