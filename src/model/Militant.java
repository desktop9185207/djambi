package model;

import java.util.HashMap;

import javax.swing.JPanel;

import java.awt.Color;

public class Militant extends Pion {

  public Militant(int posX, int posY, Color team){
    super(posX, posY, team);
    this.imgPath = "asset/militant.png";
    this.maxMinValue = new HashMap<>();
    this.maxMinValue.put("maxX", this.getPosX()+2>8 ? 8 : this.getPosX()+2);
    this.maxMinValue.put("maxY", this.getPosY()+2>8 ? 8 : this.getPosY()+2);
    this.maxMinValue.put("minX", this.getPosX()-2<0 ? 0 : this.getPosX()-2);
    this.maxMinValue.put("minY", this.getPosY()-2<0 ? 0 : this.getPosY()-2);
  }

  @Override
  public void setPosition(int posX, int posY){
    super.setPosition(posX, posY);
    this.setNewVal();
  }

  @Override
  public void checkPositionByXY(int moveX, int moveY, JPanel board){
    boolean stop = true;
    int x = this.getPosX()+moveX;
    int y = this.getPosY()+moveY;
    x = (x > 8) ? 8 : x;
    y = (y > 8) ? 8 : y;
    x = (x < 0) ? 0 : x;
    y = (y < 0) ? 0 : y;
    while((x <= this.maxMinValue.get("maxX") && y <= this.maxMinValue.get("maxY") && x >= this.maxMinValue.get("minX") && y >= this.maxMinValue.get("minY"))&& stop){
      Case tmp = (Case) board.getComponent(y*9+x);
      Pion pion = tmp.getPion();

      if(tmp.getPosX() == 4 && tmp.getPosY() == 4){
      }else if(pion==null ){
        this.possibleMoves.add(tmp.getPosistion());
      }else if(pion.team != team && pion.getIsAlive() && pion.getClass() != Chef.class){
        this.possibleMoves.add(tmp.getPosistion());
        stop = false;
      }else{
        stop = false;
      }
      x += moveX;
      y += moveY;
    }
  }

  public void setNewVal(){
    this.maxMinValue = new HashMap<>();
    this.maxMinValue.put("maxX", this.getPosX()+2>8 ? 8 : this.getPosX()+2);
    this.maxMinValue.put("maxY", this.getPosY()+2>8 ? 8 : this.getPosY()+2);
    this.maxMinValue.put("minX", this.getPosX()-2<0 ? 0 : this.getPosX()-2);
    this.maxMinValue.put("minY", this.getPosY()-2<0 ? 0 : this.getPosY()-2);
  }

}

