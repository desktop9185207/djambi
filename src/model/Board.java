package model;

import java.awt.Color;

import javax.swing.JPanel;

import view.MainVue;

public class Board extends JPanel {

  private Pion selectedPion;
  private boolean asKill = false;
  private MainVue mainVue;


  public Board(MainVue mainVue){

    this.selectedPion = null;
    this.mainVue = mainVue;
  }

  public void boardRepaint(){
    for (int i = 0; i < this.getComponentCount(); i++) {
      Case tmp = (Case) this.getComponent(i);
      if (tmp.getBackground() == Color.ORANGE) {
        tmp.setBackground(Color.white);
      } else if (tmp.getBackground() == Color.MAGENTA) {
        tmp.setBackground(tmp.getPion().getTeam());
      }
    }
  }

  public void setSelectedPion(Pion pion){
    this.selectedPion = pion;
  }

  public Pion getSelectedPion(){
    return this.selectedPion;
  }

  public boolean getAsKill(){
    return this.asKill;
  }

  public MainVue getMainVue(){
    return this.mainVue;
  }

  public void setAsKill(){
    this.asKill = !asKill;
  }


  
}
