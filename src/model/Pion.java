package model;

import java.util.ArrayList;
import java.awt.Color;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public abstract class Pion{

  protected boolean isAlive = true;
  protected HashMap<String, Integer> position;
  protected String imgPath;
  protected Color team;
  protected ArrayList<HashMap<String, Integer>> possibleMoves;
  protected HashMap<String, Integer> maxMinValue;

  public Pion (int posX, int posY, Color team){
    this.position = new HashMap<String,Integer>();
    this.position.put("X",posX);
    this.position.put("Y",posY);
    this.team = team;
    this.maxMinValue = new HashMap<>();
    this.maxMinValue.put("maxX", 8);
    this.maxMinValue.put("maxY", 8);
    this.maxMinValue.put("minX", 0);
    this.maxMinValue.put("minY", 0);
  }

  public ArrayList<HashMap<String, Integer>> scannerMove(JPanel board){
    this.possibleMoves = new ArrayList<HashMap<String, Integer>>();
    this.checkPositionByXY(0, -1, board);
    this.checkPositionByXY(0, +1, board);
    this.checkPositionByXY(-1, 0, board);
    this.checkPositionByXY(+1, 0, board);
    this.checkPositionByXY(-1, -1, board);
    this.checkPositionByXY(+1, +1, board);
    this.checkPositionByXY(-1, +1, board);
    this.checkPositionByXY(+1, -1, board);
    return this.possibleMoves;
  }

  public void checkPositionByXY(int moveX, int moveY, JPanel board){
    boolean stop = true;
    int x = this.getPosX()+moveX;
    int y = this.getPosY()+moveY;
    x = (x > 8) ? 8 : x;
    y = (y > 8) ? 8 : y;
    x = (x < 0) ? 0 : x;
    y = (y < 0) ? 0 : y;
    while((x <= this.maxMinValue.get("maxX") && y <= this.maxMinValue.get("maxY") && x >= this.maxMinValue.get("minX") && y >= this.maxMinValue.get("minY"))&& stop){
      Case tmp = (Case) board.getComponent(y*9+x);
      Pion pion = tmp.getPion();

      if(tmp.getPosX() == 4 && tmp.getPosY() == 4){
      }else if(pion==null ){
        this.possibleMoves.add(tmp.getPosistion());
      }else if(pion.team != team && pion.getIsAlive()){
        this.possibleMoves.add(tmp.getPosistion());
        stop = false;
      }else{
        stop = false;
      }
      x += moveX;
      y += moveY;
    }
  }

  public void move(Case newCase, Case oldCase){
    ImageIcon pionIcon = new ImageIcon(this.getPath());
    this.setPosition(newCase.getPosX(), newCase.getPosY());
    this.setPossibleMoves(null);
    newCase.setBackground(this.getTeam());
    newCase.setIcon(pionIcon);
    newCase.setPion(this);
    oldCase.setIcon(null);
    oldCase.setPion(null);
  }

  public void kill(Case oldCase, Case newCase, Pion pion, Board board){
    ImageIcon pionIcon = new ImageIcon(this.getPath());
    this.setPosition(newCase.getPosX(), newCase.getPosY());
    this.setPossibleMoves(null);
    pion.setPosition(oldCase.getPosX(), oldCase.getPosY());
    newCase.setBackground(this.getTeam());
    newCase.setIcon(pionIcon);
    newCase.setPion(this);
    oldCase.setIcon(null);
    oldCase.setPion(null);
    oldCase.setBackground(Color.WHITE);
    board.setSelectedPion(pion);
    board.setAsKill();
    pion.setIsAlive();
  }

  public ArrayList<HashMap<String, Integer>> getPossibleMove(){
    return this.possibleMoves;
  }

  public void setPosition(int posX, int posY){
    this.position.put("X", posX);
    this.position.put("Y", posY);
  }

  public int getPosX(){
    return this.position.get("X");
  }
  public int getPosY(){
    return this.position.get("Y");
  }
  public HashMap<String, Integer> getPosistion(){
    return this.position;
  }

  public void setPossibleMoves(ArrayList<HashMap<String, Integer>> possibleMoves){
    this.possibleMoves = possibleMoves;
  }

  public Color getTeam(){
    return this.team;
  }

  public String getPath(){
    return this.imgPath;
  }

  public boolean getIsAlive(){
    return this.isAlive;
  }

  public void setIsAlive(){
    if(isAlive){
      this.isAlive = false;
    }
    if(!isAlive){
      this.team = Color.BLACK;
    }
  }
}
