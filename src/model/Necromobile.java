package model;

import javax.swing.JPanel;

import java.awt.Color;

public class Necromobile extends Pion {

  public Necromobile(int posX, int posY, Color team){
    super(posX, posY, team);
    this.imgPath = "asset/necromobile.png";
  }

  public Necromobile(int posX, int posY, Color team , boolean isAlive){
    super(posX, posY, team);
    this.imgPath = "asset/necromobile.png";
    this.isAlive = isAlive;
  }

  @Override
  public void checkPositionByXY(int moveX, int moveY, JPanel board){
    boolean stop = true;
    int x = this.getPosX()+moveX;
    int y = this.getPosY()+moveY;
    x = (x > 8) ? 8 : x;
    y = (y > 8) ? 8 : y;
    x = (x < 0) ? 0 : x;
    y = (y < 0) ? 0 : y;
    while((x <= this.maxMinValue.get("maxX") && y <= this.maxMinValue.get("maxY") && x >= this.maxMinValue.get("minX") && y >= this.maxMinValue.get("minY"))&& stop){
      Case tmp = (Case) board.getComponent(y*9+x);
      Pion pion = tmp.getPion();

      if(tmp.getPosX() == 4 && tmp.getPosY() == 4){
      }else if(tmp.getPosX() == 4 && tmp.getPosY() == 4 && pion !=null && pion.isAlive == true ){
      }else if(tmp.getPosX() == 4 && tmp.getPosY() == 4 && pion !=null && pion.isAlive == false){
        this.possibleMoves.add(tmp.getPosistion());
      }else if(pion==null ){
        this.possibleMoves.add(tmp.getPosistion());
      }else if(pion.isAlive == false){
        this.possibleMoves.add(tmp.getPosistion());
        stop = false;
      }else{
        stop = false;
      }
      x += moveX;
      y += moveY;
    }
  }

  public void ability(){   
  }
  
}