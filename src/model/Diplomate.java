package model;

import javax.swing.ImageIcon;

import java.awt.Color;

public class Diplomate extends Pion {

  public Diplomate(int posX, int posY, Color team){
    super(posX, posY, team);
    this.imgPath = "asset/diplomate.png";
  }

  @Override
  public void kill(Case oldCase, Case newCase, Pion pion, Board board){
    ImageIcon pionIcon = new ImageIcon(this.getPath());
    this.setPosition(newCase.getPosX(), newCase.getPosY());
    this.setPossibleMoves(null);
    pion.setPosition(oldCase.getPosX(), oldCase.getPosY());
    newCase.setBackground(this.getTeam());
    newCase.setIcon(pionIcon);
    newCase.setPion(this);
    oldCase.setIcon(null);
    oldCase.setPion(null);
    oldCase.setBackground(Color.WHITE);
    board.setSelectedPion(pion);
    board.setAsKill();
  }

  public void ability(){   
  }
  
}