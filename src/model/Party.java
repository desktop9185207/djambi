package model;

import java.util.ArrayList;

public class Party {
    private ArrayList<Player> players; 
    private int currentTurn;

    public Party(ArrayList<Player> players) {
        this.players = players;
        this.currentTurn = 0;
    }

    public void passerTour() {
        currentTurn = (currentTurn + 1) % players.size();
    }

    public Player getCurrentPlayer() { 
        return players.get(currentTurn);
    }
}
