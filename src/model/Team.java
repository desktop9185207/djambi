package model;

import java.awt.Color;
import java.util.ArrayList;

public class Team {
    private Player player;
    private Color color;
    private ArrayList<Pion> teamMembers = new ArrayList<>();

    public Team(Player player, Color color) {
        this.player = player;
        this.color = color;
        this.teamMembers = new ArrayList<>();
    }


    public void changePlayerOfTeam(Player newPlayer){
        player = newPlayer;
    }


    public void addPion(Pion pion) {
        teamMembers.add(pion);
    }

    public ArrayList<Pion> getTeamMembers() {
        return teamMembers;
    }


    /*
     * Initialisation de la Team Rouge puis ajout dans TeamMembers
     */
    public static Team initTeamRed(Player player) {
        Team teamRed = new Team(player,Color.RED);

        teamRed.addPion(new Chef(0, 0, Color.RED));
        teamRed.addPion(new Assassin(1, 0, Color.RED));
        teamRed.addPion(new Militant(0, 2, Color.RED));
        teamRed.addPion(new Reporter(0, 1, Color.RED));
        teamRed.addPion(new Diplomate(1, 1, Color.RED));
        teamRed.addPion(new Militant(1, 2, Color.RED));
        teamRed.addPion(new Militant(2, 0, Color.RED));
        teamRed.addPion(new Militant(2, 1, Color.RED));
        teamRed.addPion(new Necromobile(2, 2, Color.RED));

        return teamRed;
    }





    /*
     * Initialisation de la Team Jaune puis ajout dans TeamMembers
     */
    public static Team initTeamYellow(Player player) {
        Team teamYellow = new Team(player,Color.YELLOW);
        
        teamYellow.addPion(new Chef(0, 8, Color.YELLOW));
        teamYellow.addPion(new Assassin(0, 7, Color.YELLOW));
        teamYellow.addPion(new Militant(0, 6, Color.YELLOW));
        teamYellow.addPion(new Militant(1, 6, Color.YELLOW));
        teamYellow.addPion(new Diplomate(1, 7, Color.YELLOW));
        teamYellow.addPion(new Reporter(1, 8, Color.YELLOW));
        teamYellow.addPion(new Necromobile(2, 6, Color.YELLOW));
        teamYellow.addPion(new Militant(2, 7, Color.YELLOW));
        teamYellow.addPion(new Militant(2, 8, Color.YELLOW));

        return teamYellow;
    }

    /*
     * Initialisation de la Team Green puis ajout dans TeamGreen
     */
    public static Team initTeamGreen(Player player) {
        Team teamGreen = new Team(player,Color.GREEN);
        
        teamGreen.addPion(new Chef(8, 0, Color.GREEN));
        teamGreen.addPion(new Assassin(8, 1, Color.GREEN));
        teamGreen.addPion(new Militant(8, 2, Color.GREEN));
        teamGreen.addPion(new Reporter(7, 0, Color.GREEN));
        teamGreen.addPion(new Diplomate(7, 1, Color.GREEN));
        teamGreen.addPion(new Militant(7, 2, Color.GREEN));
        teamGreen.addPion(new Militant(6, 0, Color.GREEN));
        teamGreen.addPion(new Militant(6, 1, Color.GREEN));
        teamGreen.addPion(new Necromobile(6, 2, Color.GREEN));

        return teamGreen;
    }

    /*
     * Initialisation de la Team Bleue puis ajout dans TeamBlue
     */
    public static Team initTeamBlue(Player player) {
        Team teamBlue = new Team(player,Color.BLUE);
        
        teamBlue.addPion(new Chef(8, 8, Color.BLUE));
        teamBlue.addPion(new Reporter(8, 7, Color.BLUE));
        teamBlue.addPion(new Militant(8, 6, Color.BLUE));
        teamBlue.addPion(new Assassin(7, 8, Color.BLUE));
        teamBlue.addPion(new Diplomate(7, 7, Color.BLUE));
        teamBlue.addPion(new Militant(7, 6, Color.BLUE));
        teamBlue.addPion(new Militant(6, 8, Color.BLUE));
        teamBlue.addPion(new Militant(6, 7, Color.BLUE));
        teamBlue.addPion(new Necromobile(6, 6, Color.BLUE));

        return teamBlue;
    }



    /* 
     * Initialisation de la partie à 4
     */
    public static ArrayList<Team> init4Teams(Player player1, Player player2, Player player3, Player player4) {
        ArrayList<Team> teams = new ArrayList<>();
        teams.add(initTeamRed(player1));
        teams.add(initTeamBlue(player2));
        teams.add(initTeamGreen(player3));
        teams.add(initTeamYellow(player4));
        return teams;
    }

    /*
     * Initialisation de la partie à 2
     */
    public static ArrayList<Team> init2Teams(Player player1, Player player2) {
        ArrayList<Team> teams = new ArrayList<>();
        teams.add(initTeamYellow(player1));
        teams.add(initTeamGreen(player1));
        teams.add(initTeamBlue(player2));
        teams.add(initTeamRed(player2));
        return teams;
    }


   
}
