package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.MainController;

public class RulesVue {
    private static JFrame fenetre;

    public static void showRules() {

        fenetre = new JFrame();
        fenetre.setTitle("Règles Djambi");
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.setSize(700, 700);

        JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.setBackground(Color.lightGray);
        contentPane.setBorder(new EmptyBorder(20, 20, 20, 20)); // Ajout d'un espacement en haut

        String htmlContent = "<html><h1>Règlement Djambi</h1><h3>Chaque joueur possède :</h3><p><ul><li>1 Chef</li><li>1 Assassin</li><li>1 Reporter</li><li>1 Nécromobile</li><li>1 Diplomate</li><li>4 Militants</li></ul></p><p>Pour des soucis de simplification, nous n'implémenterons que le mode 2 et 4 joueurs. Mode 2 joueurs : Chaque joueur contrôle, dès le départ, deux couleurs aléatoirement réparties.</p><p>Le jeu se joue sur un plateau de 9×9 cases. La case centrale est nommée Labyrinthe. Aucune pièce, à part le Chef ne peut s’y arrêter.</p><p>Voici les positions de départ ainsi que le plateau.</p><h3>But du jeu</h3><p>Un peu comme aux échecs, on joue chacun son tour, l'objectif étant d'éliminer tous les rivaux. Deux façons possibles :</p><ul><li>En tuant les Chefs ennemis.</li></ul><p>Un joueur qui tue un chef s’approprie ses pièces.</p><li>En encerclant les Chefs ennemis.</li><p>Les pièces d’un Chef encerclé sont récupérées par le 1er joueur à occuper le Labyrinthe. Vous pourrez encercler le chef ennemi par des cadavres. Celà sera expliqué plus tard.</p><br><br><h3>Les pions</h3><br /><p>Il en existe deux types :</p><ul><li>Tueurs : Chef, Militant, Assassin et Reporter.</li><li>Les Déplaceurs : Diplomate et Nécromobile.</li></ul><p>La particularité de ce jeu réside grandement dans le mécanisme suivant. Lorsqu'un pion A tue un pion B d'une couleur adverse, le pion B est retourné face mort et placé sur le plateau, sur un emplacement libre, selon les désirs du joueur détenteur du pion A.</p><p>Les pions se déplacent en diagonales, en lignes ou en colonnes. n'importe quelle pièce vivante ou morte constitue un obstacle infranchissable.</p><h4>Le Chef</h4><ul><li>Type : Tueur Déplacement : Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il veut</li><li>Élimination : Peut tuer des pièces en occupant leur cases. La pièce tuée est retournée et placée sur n’importe quelle case libre du plateau.</li><li>Pouvoir : Peut occuper le Labyrinthe (les autres pièces ne font que le traverser). Lorsqu’il occupe le labyrinthe, le Chef dispose d’un tour de jeu supplémentaire après chaque joueur.</li></ul><h4>Le Militant</h4><ul><li>Type : Tueur</li><li>Déplacement : Contrairement aux autres pièces qui se déplacent d’autant de cases qu’elles veulent, le Militant ne parcourt que 2 cases maximum, dans toutes les directions.</li><li>Élimination : Peut tuer des pièces en occupant leur case. La pièce tuée est retournée et placée sur n’importe quelle case libre du plateau. Le militant est le seul (parmi les tueurs : Chef, Assassin et Reporter) à ne pas pouvoir tuer un Chef.</li></ul><h4>L'Assassin</h4><ul><li>Type : Tueur</li><li>Déplacement : Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il veut</li><li>Élimination : Peut tuer des pièces en occupant leur cases. La pièce tuée est retournée et placée sur la case qu’occupait l’Assassin avant son méfait.</li><li>Pouvoir : Peut occuper le labyrinthe pour tuer un Chef. Il dispose alors d’un tour supplémentaire pour fuir le labyrinthe (aucune pièce à part le Chef ne peut rester dans le labyrinthe).</li></ul><h4>Le Reporter</h4><ul><li>Type : Tueur</li><li>Déplacement : Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il faut</li><li>Élimination : Tue après son déplacement la pièce adjacente à l’un de ses côtés (au choix). Les pièces tuées ne sont pas déplacées. Il n'est pas obligé de tuer. Ne peut tuer s’il est utilisé par un diplomate ennemi.</li></ul><h4>Le Diplomate</h4><ul><li>Type : Deplaceur</li><li>Déplacement : Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il faut</li><li>Élimination : N’élimine pas !</li><li>Pouvoir : Lorsque le diplomate atterrit sur une pièce ennemie, cette dernière est alors replacée par le diplomate sur la case libre de son choix. Attention il ne peut agir qu'avec des pièces d'une autre couleur que la sienne.</li></ul><h4>La Nécromobile</h4><ul><li>Type : Deplaceur</li><li>Déplacement : Il se déplace dans toutes les directions (comme une dame aux échecs), d'autant de cases qu'il faut. C'est le seul pion pouvant s'arrêter sur un mort.</li><li>Élimination : N’élimine pas !</li><li>Pouvoir : Lorsque le diplomate atterrit sur une pièce contenant un mort, cette dernière est alors replacée par le diplomate sur la case libre de son choix. La nécromobile peut venir \"nettoyer\" le labyrinthe, il dispose alors d’un tour supplémentaire pour fuir le labyrinthe (aucune pièce à part le Chef ne peut rester dans le labyrinthe).</li></ul></html>";

        // Création JLabel
        JLabel text1Label = new JLabel(htmlContent);
        text1Label.setVerticalAlignment(SwingConstants.TOP); 

        // JScroll
        JScrollPane scrollPane = new JScrollPane(text1Label);
        scrollPane.setPreferredSize(new Dimension(650,650)); 

        contentPane.add(scrollPane, BorderLayout.CENTER);

        // Définir taille & police
        JButton boutonRetourMenu = new JButton("Retour Menu");
        Dimension boutonDimension = new Dimension(200, 50);
        boutonRetourMenu.setPreferredSize(boutonDimension);
    
        Font boutonFont = new Font(boutonRetourMenu.getFont().getName(), Font.PLAIN, 20);
        boutonRetourMenu.setFont(boutonFont);

        // Event Retour Menu
        boutonRetourMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainController jeuController = new MainController();
                jeuController.startMenu();
    
                fenetre.dispose();
            }
        });
        contentPane.add(boutonRetourMenu, BorderLayout.SOUTH);

        // Ajout du contentPane à la fenêtre
        fenetre.setContentPane(contentPane); 

        fenetre.setResizable(false);
        fenetre.setVisible(true);
    }
}
