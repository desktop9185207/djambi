package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.*;
import javax.swing.border.LineBorder;

import controller.EventController;
import model.Pion;
import model.Player;
import model.Team;
import model.Case;
import model.Party;
import model.Board;

public class MainVue {
    private JFrame fenetre;
    private Board grillePanel;
    private Party partieDjambi;
    private JLabel statusLabel;

    public MainVue(){
        this.statusLabel = new JLabel();
    }

    public void initPlayers4() {
        // Ajout Joueur
        Player player1 = new Player("1", new ArrayList<>(Arrays.asList(Color.RED)));
        Player player3 = new Player("2", new ArrayList<>(Arrays.asList(Color.BLUE)));
        Player player2 = new Player("3", new ArrayList<>(Arrays.asList(Color.GREEN)));
        Player player4 = new Player("4", new ArrayList<>(Arrays.asList(Color.YELLOW)));
        // Ajout Team
        ArrayList<Team> teams = Team.init4Teams(player1, player2, player3, player4);

        // Créer une liste de joueurs
        ArrayList<Player> joueurs = new ArrayList<>();
        joueurs.add(player1);
        joueurs.add(player2);
        joueurs.add(player3);
        joueurs.add(player4);
        statusLabel.setText("<html>C'est au joueur <font color='red'>Rouge</font> de jouer</html>");

        // Créer une partie avec la liste de joueurs
        this.partieDjambi = new Party(joueurs);
        displayTeams(teams);
    }


    public void initPlayers2() {
        // Ajout Joueur
        Player player1 = new Player("1", new ArrayList<>(Arrays.asList(Color.RED, Color.BLUE)));
        Player player2 = new Player("2", new ArrayList<>(Arrays.asList(Color.YELLOW, Color.GREEN)));
        // Ajout Team
        Team.init2Teams(player1, player2);
        ArrayList<Team> teams = Team.init2Teams(player1, player2);

        // Créer une liste de joueurs
        ArrayList<Player> joueurs = new ArrayList<>();
        joueurs.add(player1);
        joueurs.add(player2);
        statusLabel.setText("<html>C'est au joueur <font color='red'>Rouge</font> / <font color='blue'>Bleu</font> de jouer</html>");


        // Créer une partie avec la liste de joueurs
        this.partieDjambi = new Party(joueurs);
        displayTeams(teams);
    }

    public void initFenetre() {
        fenetre = new JFrame();
        fenetre.setTitle("Djambi");
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Grille
        grillePanel = new Board(this);
        grillePanel.setLayout(new GridLayout(9, 9));
        grillePanel.setPreferredSize(new Dimension(700, 700));
        grillePanel.setBackground(Color.white);
        
        // Barre qui jouer
        JPanel statusPanel = new JPanel();
        statusPanel.setPreferredSize(new Dimension(600, 40));
        statusPanel.setBackground(Color.lightGray);

        // Texte pour Barre qui jouer
        
        this.statusLabel = new JLabel("");

        statusLabel.setHorizontalAlignment(SwingConstants.CENTER);
        statusLabel.setFont(statusLabel.getFont().deriveFont(statusLabel.getFont().getSize() * 1.5f));
        statusPanel.add(statusLabel);

        // Rangement Grille
        fenetre.add(grillePanel, BorderLayout.CENTER);
        fenetre.add(statusPanel, BorderLayout.SOUTH);

        // Bloquer dimension
        fenetre.setResizable(false);

        fenetre.pack();
        fenetre.setVisible(true);
    }

    

    public void displayTeams(ArrayList<Team> teams) {
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                // Plateau
                Case cellLabel = new Case(x, y);
                cellLabel.addActionListener(new EventController(grillePanel,partieDjambi));
                cellLabel.setHorizontalAlignment(SwingConstants.CENTER);
                cellLabel.setVerticalAlignment(SwingConstants.CENTER);
                cellLabel.setBorder(new LineBorder(Color.BLACK));
                cellLabel.setBackground(Color.WHITE);
                grillePanel.add(cellLabel);
                // Parcourir Teams pour trouver les pions
                for (Team team : teams) {
                    ArrayList<Pion> teamMembers = team.getTeamMembers();
                    for (Pion pion : teamMembers) {
                        if (x == pion.getPosX() && y == pion.getPosY()) {
                            ImageIcon pionIcon = new ImageIcon(pion.getPath());
                            cellLabel.setIcon(pionIcon);
                            cellLabel.setBackground(pion.getTeam());
                            cellLabel.setPion(pion);
                        }
                    }
                }

                // Labyrinthe
                if (x == 4 && y == 4) {
                    String imageUrl = "asset/chef.png";
                    ImageIcon icon = new ImageIcon(imageUrl);
                    
                    cellLabel.setIcon(icon);
                    cellLabel.setOpaque(true);
                    cellLabel.setBackground(Color.GRAY);
                }
            }
        }
    }

    public String currentTeam() {
        ArrayList<Color> colors = partieDjambi.getCurrentPlayer().getColors();
        
        // Chaine pour stocker valeurs
        StringBuilder teamColors = new StringBuilder();
        
    
        for (Color color : colors) {
            switch (color.toString()){
                case "java.awt.Color[r=255,g=0,b=0]" : 
                    teamColors.append("<font color='red'>Rouge</font>").append(" / ");
                    break; 
                case "java.awt.Color[r=0,g=0,b=255]" : 
                    teamColors.append("<font color='blue'>Bleu</font>").append(" / ");
                    break;
                case "java.awt.Color[r=0,g=255,b=0]" : 
                    teamColors.append("<font color='green'>Vert</font>").append(" / ");
                    break;
                case "java.awt.Color[r=255,g=255,b=0]" : 
                    teamColors.append("<font color='yellow'>Jaune</font>").append(" / ");
                    break;
                default : 
                    teamColors.append("Autre").append(" / "); 
                    break;
            }
        }
        
        // Supprimer le /
        if (teamColors.length() > 0) {
            teamColors.delete(teamColors.length() - 3, teamColors.length()); 
        }
    
        return teamColors.toString();
    }
    
    public void printNextPlayer() {
        statusLabel.setText("<html>C'est au joueur " + currentTeam() + " de jouer</html>");
    }
}