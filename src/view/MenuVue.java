package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import controller.MainController;

public class MenuVue {

    private JFrame fenetre;
    private JPanel containPanel;

    public void initialiserFenetre(){
        fenetre = new JFrame();
        fenetre.setTitle("Menu Djambi");
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
        containPanel = new JPanel();
        containPanel.setPreferredSize(new Dimension(600, 200));
        containPanel.setBackground(Color.lightGray);
    
        JPanel text1Panel = new JPanel();
        text1Panel.setPreferredSize(new Dimension(600, 150));
        text1Panel.setBackground(Color.lightGray);
    
        JPanel RulesPanel = new JPanel();
        RulesPanel.setPreferredSize(new Dimension(100, 100));
        RulesPanel.setBackground(Color.lightGray);
    
        // Ajouter un espace vide + texte
        JLabel text1Label = new JLabel("<html><div style='margin-top: 24px; font-size:20px;text-align: center; line-height: 100%;'><p>Bienvenue,</p><p> Préparez-vous à jouer à DJAMBI</p></div></html>");
        //Rangement titre
        text1Label.setHorizontalAlignment(SwingConstants.CENTER);
        text1Label.setVerticalAlignment(SwingConstants.CENTER);
        text1Panel.add(text1Label);
    
        //Bouton Choix
        JButton bouton2 = new JButton("2 Joueurs");
        JButton bouton4 = new JButton("4 Joueurs");
    
        // Définir une taille & police perso pour les boutons
        Dimension boutonDimension = new Dimension(200, 50);
        bouton2.setPreferredSize(boutonDimension);
        bouton4.setPreferredSize(boutonDimension);
    
        Font boutonFont = new Font(bouton2.getFont().getName(), Font.PLAIN, 20);
        bouton2.setFont(boutonFont);
        bouton4.setFont(boutonFont);

        // Event Jouer à 2
        bouton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainController jeuController = new MainController();
                jeuController.startGame2();
    
                fenetre.dispose();
            }
        });
        
    
        // Event Jouer à 4
        bouton4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainController jeuController = new MainController();
                jeuController.startGame4();
    
                fenetre.dispose();
            }
        });
    
        JButton rulesButton = new JButton("Règles");
        rulesButton.setFont(boutonFont);
        // Event règles

        // Event Reglement
        rulesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                RulesVue.showRules();
    
                fenetre.dispose();
            }
        });


        //Rangement titre
        rulesButton.setHorizontalAlignment(SwingConstants.CENTER);
        rulesButton.setVerticalAlignment(SwingConstants.CENTER);
        RulesPanel.add(rulesButton);
    
        // Organisation composants dans la fenêtre
        fenetre.setLayout(new BorderLayout());
    
        // Rangement
        fenetre.add(text1Panel, BorderLayout.NORTH);
        fenetre.add(containPanel, BorderLayout.CENTER);
        fenetre.add(RulesPanel, BorderLayout.SOUTH);
    
        // Ajoutez boutons au panneau containPanel
        containPanel.add(bouton2);
        containPanel.add(bouton4);
    
        // Bloquer dimension
        fenetre.setResizable(false);
        fenetre.pack();
        
        fenetre.setSize(700, 700);
        fenetre.setVisible(true);
    }
    
}
