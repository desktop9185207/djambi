package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.HashMap;

import javax.swing.ImageIcon;

import java.util.ArrayList;

import model.Board;
import model.Case;
import model.Party;
import model.Pion;

public class EventController implements ActionListener {

  private Board board;
  private Party party;

  public EventController(Board board, Party party) {
    this.board = board;
    this.party = party;
  }

  public boolean bonneCouleur(Case tuile) {
    Pion pion = tuile.getPion();
    if (pion == null) {
      if (tuile.getBackground() == Color.ORANGE) {
        return true;
      }
      return false;
    } else {
      if (tuile.getBackground() == Color.MAGENTA || tuile.getBackground() == Color.BLACK) {
        return true;
      }
      Color couleurPion = pion.getTeam();
      // Parcourez la liste des couleurs du joueur actuel
      for (Color couleurJoueur : party.getCurrentPlayer().getColors()) {
        // Vérifiez si la couleur du joueur correspond à la couleur du pion
        if (couleurJoueur.equals(couleurPion)) {
          return true;
        }
      }
      return false;
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    boolean asMoved = false;
    Case newCase = (Case) e.getSource();
    Pion pion = newCase.getPion();
    Pion selectedPion = this.board.getSelectedPion();
    // Si on a tué un pion
    if (!bonneCouleur(newCase) || this.board.getAsKill() ) {
      if (this.board.getAsKill()) {
        if (pion == null && this.board.getSelectedPion() != null && newCase.getBackground() == Color.ORANGE) {
          selectedPion.setPosition(newCase.getPosX(), newCase.getPosY());
          ImageIcon pionIcon = new ImageIcon(selectedPion.getPath());
          newCase.setIcon(pionIcon);
          newCase.setPion(selectedPion);
          newCase.setBackground(this.board.getSelectedPion().getTeam());
          if (this.board.getAsKill()) {
            this.board.setAsKill();
            this.party.passerTour();
            this.board.getMainVue().printNextPlayer();
            this.board.boardRepaint();
          }
        }
      }
    } else {
      // Si on selectione un pion
      if (selectedPion != null) {
        if (selectedPion.getPossibleMove() != null) {
          // si la case selectionne est dans les moves possibles du pion selectionne
          if (selectedPion.getPossibleMove().contains(newCase.getPosistion())) {
            Case oldCase = (Case) board.getComponent(selectedPion.getPosY() * 9 + selectedPion.getPosX());
            if (newCase.getBackground() == Color.ORANGE) {
              selectedPion.move(newCase, oldCase);
              oldCase.setBackground(Color.WHITE);
              this.board.setSelectedPion(null);
              this.party.passerTour();
              this.board.getMainVue().printNextPlayer();
              asMoved = true;
            } else if (newCase.getBackground() == Color.MAGENTA) {
              selectedPion.kill(oldCase, newCase, pion, board);
            }
          }
        }
      }
      if (this.board.getAsKill()) {
        for (int i = 0; i < this.board.getComponentCount(); i++) {
          Case tmp = (Case) board.getComponent(i);
          if (tmp.getBackground() == Color.WHITE) {
            tmp.setBackground(Color.ORANGE);
          } else if (tmp.getBackground() == Color.MAGENTA) {
            tmp.setBackground(tmp.getPion().getTeam());
          }
        }
      } else {
        ArrayList<HashMap<String, Integer>> possibleMove = new ArrayList<HashMap<String, Integer>>();
        
        this.board.boardRepaint();
        if (pion != null && !asMoved) {
          if (pion.getIsAlive()) {
            possibleMove = pion.scannerMove(this.board);
            this.board.setSelectedPion(pion);
            for (int i = 0; i < possibleMove.size(); i++) {
              int x = possibleMove.get(i).get("X");
              int y = possibleMove.get(i).get("Y");
              Case tmp = (Case) board.getComponent((y * 9) + x);
              if (tmp.getPion() != null) {
                tmp.setBackground(Color.magenta);
              } else {
                tmp.setBackground(Color.orange);
              }
            }
          }
        }
      }
      if (selectedPion != null && pion == null && newCase.getBackground() != Color.MAGENTA) {
        this.board.setSelectedPion(null);
      }
    }

    this.board.revalidate();
    this.board.repaint();
  }

  public boolean pionInLabyrinthe(Pion pion){
    if(pion.getPosX()==4 && pion.getPosY()==4){
      return true;
    }else{
      return false;
    }
  }


}
