package controller;

import view.MainVue;
import view.MenuVue;

public class MainController {
    private MainVue mainVue;
    private MenuVue menuVue;

    public MainController() {
        menuVue = new MenuVue();
        mainVue = new MainVue();
    }

    public void startMenu() {
        menuVue.initialiserFenetre();
    }

    public void startGame2() {
        // Initialialise
        mainVue.initFenetre();
        mainVue.initPlayers2();
        
    }

    public void startGame4() {
        // Initialialise
        mainVue.initFenetre();
        mainVue.initPlayers4();
        

        
    }
}